<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>Illustration</title>
	<link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>
	<link href='styles/simplelightbox.min.css' rel='stylesheet' type='text/css'>
	<link href='styles/demo.css' rel='stylesheet' type='text/css'>
	<?php include "headd.php"; ?>
</head>
<body>
	<div class="container">
		<?php include "headerr.php"; ?>
		<h1 class="align-center">~ illustration ~</h1>
		<div class="gallery">
			<a href="images/illustration/5.jpg"><img src="images/illustration_thumbnails/5.jpg" alt="" /></a>
			<a href="images/illustration/7.jpg"><img src="images/illustration_thumbnails/7.jpg" alt="" /></a>
			<a href="images/illustration/8.jpg"><img src="images/illustration_thumbnails/8.jpg" alt=""/></a>
		
			<a href="images/illustration/10.jpg"><img src="images/illustration_thumbnails/10.jpg" alt=""/></a>

			<a href="images/illustration/13.jpg"><img src="images/illustration_thumbnails/13.jpg" alt=""/></a>
			<a href="images/illustration/1.jpg"><img src="images/illustration_thumbnails/1.jpg" alt="" /></a>
			<a href="images/illustration/11.jpg"><img src="images/illustration_thumbnails/11.jpg" alt="" /></a>
		
			<a href="images/illustration/6.jpg"><img src="images/illustration_thumbnails/6.jpg" alt="" /></a>
			<a href="images/illustration/12.jpg"><img src="images/illustration_thumbnails/12.jpg" alt="" /></a>
			<a href="images/illustration/4.jpg"><img src="images/illustration_thumbnails/4.jpg" alt="" /></a>
			<a href="images/illustration/2.jpg"><img src="images/illustration_thumbnails/2.jpg" alt="" /></a>
			<a href="images/illustration/3.jpg"><img src="images/illustration_thumbnails/3.jpg" alt="" /></a>
				<a href="images/illustration/rukantys.jpg" class="big"><img src="images/illustration_thumbnails/rukantys.jpg" alt="" /></a>
			<a href="images/illustration/9.jpg"><img src="images/illustration_thumbnails/9.jpg" alt="" /></a>
		
			

			<div class="clear"></div> 

		</div>
		
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script type="text/javascript" src="scripts/simple-lightbox.js"></script>
	<script>
		$(function(){
			var $gallery = $('.gallery a').simpleLightbox({
				closeText: 'X',
				throttleInterval: 0
			});

		});
	</script>
</body>
<?php include "footer.php"; ?>
</html>