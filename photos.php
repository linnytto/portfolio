<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>Important</title>
	<link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>
	<link href='styles/simplelightbox.min.css' rel='stylesheet' type='text/css'>
	<link href='styles/demo.css' rel='stylesheet' type='text/css'>
	<?php include "headd.php"; ?>
</head>
<body>
	<div class="container">
		<?php include "headerr.php"; ?>
		<h1 class="align-center">~ what's important ~</h1>
		<div class="gallery">
			<a href="images/memories/1.jpg" class="big"><img src="images/memories/1.jpg" alt="" /></a>
			<a href="images/memories/2.jpg"><img src="images/memories/2.jpg" alt="" /></a>
			<a href="images/memories/3.jpg"><img src="images/memories/3.jpg" alt="" /></a>
			<a href="images/memories/4.jpg"><img src="images/memories/4.jpg" alt="" /></a>
			<a href="images/memories/5.jpg"><img src="images/memories/5.jpg" alt="" /></a>
		<center><p>I’ve got a cold abroad <br>
	Malmö, Sweden / <br>
København, Denmark <br>
 2018</p></center>
			<div class="clear"></div>
			<a href="images/memories/6.jpg"><img src="images/memories_thumbnails/6.jpg" alt="" /></a>
			<a href="images/memories/7.jpg"><img src="images/memories_thumbnails/7.jpg" alt="" /></a>
			<a href="images/memories/16.jpg"><img src="images/memories_thumbnails/16.jpg" alt=""/></a>
			<a href="images/memories/15.jpg"><img src="images/memories_thumbnails/15.jpg" alt="" /></a> 
			<center><p>'1928' (2018) <br> photographic project dedicated to my grandfather (1928-2017) who's been living in a small Lithuanian town where I've been spending a lot of time as well. <br> Grandfather was born in Ukraine, survived the second world war, concentration</center> <align-right> camps, and managed to become a very well known doctor when he moved to Lithuania.</align-right> After his retirement, he continued living in his old house. In 2009 when I first got a very shit camera, I've started following him around and taking photos - this is basically how I got into documentary style photography that I've been interested in a lot while living in Lithuania. 
My project is showing the daily life of old people left living alone in empty villages, as the younger generation is moving into bigger towns or other countries. These people are neglecting the innovations, preserving old traditions, and still giving a warm welcome to every single guest. Everything they get now is nostalgic memories of their prospering towns in the past. <br>
My aim is to make people remember and respect their old relatives, and support those in need.</p>
			<div class="clear"></div>
			
			<a href="images/memories/8.jpg"><img src="images/memories_thumbnails/8.jpg" alt="" /></a>
			<a href="images/memories/9.jpg"><img src="images/memories_thumbnails/9.jpg" alt="" /></a>
			
			<a href="images/memories/11.jpg"><img src="images/memories_thumbnails/11.jpg" alt="" /></a>
			<a href="images/memories/12.jpg"><img src="images/memories_thumbnails/12.jpg" alt="" /></a>
			<a href="images/memories/13.jpg"><img src="images/memories_thumbnails/13.jpg" alt="" /></a>
			<center><p>"Lina, take a picture dedicated to me, when you go to Lithuania"</p></center> 
			<center></center><p>Photos I took on Fios camera. The picture of pepper was taken for Fio especially. The last picture is Fio showing me the map of San Francisco. This was Fio's last summer. 2018. </p></center>
			<div class="clear"></div>


			<a href="images/memories/14.jpg"><img src="images/memories_thumbnails/14.jpg" alt="" /></a>
			<a href="images/memories/10.jpg"><img src="images/memories_thumbnails/10.jpg" alt="" /></a> 
			<div class="clear"></div><p>A trip to Madrid with mum, 2018.</p>
			
			
						
			<div class="clear"></div>

		</div>
		
	</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="scripts/simple-lightbox.js"></script>
<script>
	$(function(){
		var $gallery = $('.gallery a').simpleLightbox({
			closeText: 'X',
			throttleInterval: 0
		});

	});
</script>
</body>
<?php include "footer.php"; ?>
</html>