<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>Friends</title>
	<link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>
	<link href='styles/simplelightbox.min.css' rel='stylesheet' type='text/css'>
	<link href='styles/demo.css' rel='stylesheet' type='text/css'>
	<?php include "headd.php"; ?>
</head>
<body>
	<div class="container">
		<?php include "headerr.php"; ?>
		<h1 class="align-center">~ go home you've had enough ~</h1>
		<div class="gallery">
			<a href="images/friends/4.jpg"><img src="images/friends_thumbnails/4.jpg" alt=""/></a>
			<a href="images/friends/1.jpg"><img src="images/friends_thumbnails/1.jpg" alt=""/></a>
			<a href="images/friends/2.jpg"><img src="images/friends_thumbnails/2.jpg" alt=""></a>
			<a href="images/friends/10.jpg"><img src="images/friends_thumbnails/10.jpg" alt="" ></a>
			<a href="images/friends/12.jpg"><img src="images/friends_thumbnails/12.jpg" alt=""></a>
			<a href="images/friends/5.jpg"><img src="images/friends_thumbnails/5.jpg" alt=""/></a>
			<a href="images/friends/16.jpg"><img src="images/friends_thumbnails/16.jpg" alt=""></a>
			<a href="images/friends/9.jpg"><img src="images/friends_thumbnails/9.jpg" alt="" ></a>
			<a href="images/friends/11.jpg"><img src="images/friends_thumbnails/11.jpg" alt=""></a>
			<a href="images/friends/13.jpg"><img src="images/friends_thumbnails/13.jpg" alt=""></a>
			<a href="images/friends/3.jpg"><img src="images/friends_thumbnails/3.jpg" alt="" ></a>
			<a href="images/friends/15.jpg"><img src="images/friends_thumbnails/15.jpg" alt="" ></a>
			<a href="images/friends/6.jpg"><img src="images/friends_thumbnails/6.jpg" alt=""></a>
			<a href="images/friends/7.jpg"><img src="images/friends_thumbnails/7.jpg" alt=""></a>
			<a href="images/friends/8.jpg"><img src="images/friends_thumbnails/8.jpg" alt="" ></a>
			
			
			<div class="clear"></div>

		</div>
		
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script type="text/javascript" src="scripts/simple-lightbox.js"></script>
	<script>
		$(function(){
			var $gallery = $('.gallery a').simpleLightbox({
				closeText: 'X',
				throttleInterval: 0
			});

		});
	</script>
</body>
<?php include "footer.php"; ?>
</html>