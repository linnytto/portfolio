<!DOCTYPE html>
<html>
<head>
	<title>gallery</title>
	
	<link rel="stylesheet" href="styles/style.css" type="text/css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
</head>
<?php include "headd.php"; ?>
<body>
<div class="gallerycontainer">

<?php include "headerr.php"; ?>
<div class="row gallerygridcontainer">
      <div class="col s12 l4"><a href="bands.php">
<div class="bandshover">
  <img class="bandsimage" src="images/bands/9.jpg">
  <div class="imagehover">
    <p class="bandshovertext">bands</p>
  </div></a></div></div>

      <div class="col s12 l4"><a href="friends.php">
<div class="friendshover">
  <img class="friendsimage" src="images/friends/3.jpg">
  <div class="imagehover">
    <p class="friendshovertext">go home you've had enough</p>
  </div></a></div></div>

      <div class="col s12 l4"><a href="photos.php">
<div class="otherphotoshover img">
  <img class="randomimage" src="images/memories/16.jpg">
  <div class="imagehover">
    <p class="randomhovertext">other images</p>
  </div></a></div></div>


      <div class="col s12 l4"><a href="illustration.php">
<div class="illustrationhover">
  <img class="illustrationimage" src="images/illustration/12.jpg">
  <div class="imagehover">
    <p class="illustrationhovertext">illustration</p>
  </div></a></div></div>
</div>
    
</div>
</body>
<?php include "footer.php"; ?>

</html>