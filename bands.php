<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>Bands</title>
	<link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>
	<link href='styles/simplelightbox.min.css' rel='stylesheet' type='text/css'>
	<link href='styles/demo.css' rel='stylesheet' type='text/css'>
<?php include "headd.php"; ?>
</head>

<body>
	<div class="container">
		<?php include "headerr.php"; ?>
		<h1 class="align-center">~ bands ~</h1>
		<div class="gallery">
			<a href="images/bands/1.jpg"><img src="images/bands_thumbnails/1.jpg" alt="Aiste" title="Aiste, instagram @alovebuzz"/></a></a>
			<a href="images/bands/2.jpg"><img src="images/bands_thumbnails/2.jpg" alt="black midi" title="black midi @ Windmill Brixton, instagram @bmblackmidi"/></a>
			<a href="images/bands/19.jpg"><img src="images/bands_thumbnails/19.jpg" alt="Rodents" title="Rodents @ Windmill, Brixton, facebook @Rodentsss"/></a>
			<a href="images/bands/20.jpg"><img src="images/bands_thumbnails/20.jpg" alt="SCUD FM" title="SCUD FM @ Windmill, Brixton, facebook https://www.facebook.com/SCUD-FM-548164638902088/"/></a>
			<a href="images/bands/21.jpg"><img src="images/bands_thumbnails/21.jpg" alt="Waterbaby" title="Waterbaby, instagram @waterbabytv"/></a> 
			<a href="images/bands/18.jpg"><img src="images/bands_thumbnails/18.jpg" alt="PVA" title="PVA @ The Bunker, Deptford, instagram @pva_are_ok"/></a>
			<a href="images/bands/5.jpg"><img src="images/bands_thumbnails/5.jpg" alt="Don't Worry" title="Don't Worry @ The Old Blue Last, instagram @dontworryband"/></a>
			<a href="images/bands/16.jpg"><img src="images/bands_thumbnails/16.jpg" alt="Peeping Drexels" title="Peeping Drexels @ The Social, instagram @peepingdrexels"/></a>
			<a href="images/bands/3.jpg"><img src="images/bands_thumbnails/3.jpg" alt="black midi" title="black midi @ Windmill, Brixton, instagram @bmblackmidi"/></a>
				<a href="images/bands/11.jpg"><img src="images/bands_thumbnails/11.jpg" alt="Mysteron" title="Mysteron @ The Five Bells, New Cross, instagram @mysteronmusic"/></a>
			<a href="images/bands/12.jpg"><img src="images/bands_thumbnails/12.jpg" alt="Mysteron" title="Mysteron @ The Five Bells, New Cross, instagram @mysteronmusic"/></a>
			<a href="images/bands/14.jpg"><img src="images/bands_thumbnails/14.jpg" alt="Peeping Drexels" title="Peeping Drexels @ The Social, instagram @peepingdrexels"/></a>
				<a href="images/bands/17.jpg"><img src="images/bands_thumbnails/17.jpg" alt="PVA" title="PVA @ The Bunker, Deptford, instagram @pva_are_ok"/></a>
				<a href="images/bands/10.jpg"><img src="images/bands_thumbnails/10.jpg" alt="MUMMY" title="MUMMY @ The NINES Peckham, instagram @weareyourmummy"/></a>
			<a href="images/bands/13.jpg"><img src="images/bands_thumbnails/13.jpg" alt="Mysteron" title="Mysteron @ The Five Bells, New Cross, instagram @mysteronmusic"/></a>
			<a href="images/bands/15.jpg"><img src="images/bands_thumbnails/15.jpg" alt="Peeping Drexels" title="Peeping Drexels @ The Social, instagram @peepingdrexels"/></a>
			<a href="images/bands/4.jpg"><img src="images/bands_thumbnails/4.jpg" alt="Dear Man" title="Dear Man, instagram @dearmanband"/></a>	
			<a href="images/bands/6.jpg" ><img src="images/bands_thumbnails/6.jpg" alt="Fontaines D.C." title="Fontaines D.C @ Windmill, Brixton, instagram @fontainesband"/></a>
			<a href="images/bands/9.jpg"><img src="images/bands_thumbnails/9.jpg" alt="MUMMY" title="MUMMY @ The NINES Peckham, instagram @weareyourmummy"/></a>
			<a href="images/bands/22.jpg"><img src="images/bands_thumbnails/22.jpg" alt="YOWL" title="YOWL @ Windmill, Brixton, instagram @yowlmusic"/></a>
			<div class="clear"></div>
					</div>


	</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="scripts/simple-lightbox.js"></script>
<script>
	$(function(){
		var $gallery = $('.gallery a').simpleLightbox({
			closeText: 'X',
			throttleInterval: 0
		});

	});
</script>
</body>
<?php include "footer.php"; ?>

</html>
