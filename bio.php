<!DOCTYPE html>
<html>
<head>
	<title>contact info</title>
	<meta charset="utf-8">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link rel="stylesheet" type="text/css" href="styles/style.css">

<?php

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "portfolio_forms";

    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);

    // Check connection
    if (!$conn) {

        die("Connection failed: " . mysqli_connect_error());
    }

    mysqli_set_charset($conn, "utf8");

    // echo "Viskas tvarkoj! <br><br>";


    // print_r($_GET);
    // echo $_GET["veikla"];





    if (isset($_GET["firstname"]) && $_GET["lastname"] != "") {

      $sql = "INSERT INTO forms (name, surname, email, message)
      VALUES ('" . $_GET["firstname"] . "', 
          '" . $_GET["lastname"] . "', 
          '" . $_GET["email"] . "',
          '" . $_GET["textarea"] . "')";

      echo $sql; 

      if (mysqli_query($conn, $sql)) {
          echo "New record created successfully";
          header('Location: bio.php');
      } else {
          echo "Error: " . $sql . "<br>" . mysqli_error($conn);
          header('Location: bio.php');
      }

     	$to      = 'lina.zabinskaja@gmail.com';
		$subject = 'You received a message from Your Portfolio page';
		$message = $_GET["textarea"];
		$headers = "From: " . $_GET["email"] . "\r\n" .
    	"Reply-To: " . $_GET["email"] . "\r\n" .
    	'X-Mailer: PHP/' . phpversion();

		mail($to, $subject, $message, $headers);



    }
    ?>



</head>
<body>
	<div class="container mano_container">
	<?php include "headerr.php"; ?>


<h1 class="biografija">about me</h1>

<p class="istorija">
	Lina Zabinskaya <br>
	University of Westminster’17 -Contemporary Media Practice BA
</p>


<h2 class="formos">contact form</h2>

<div class="container">

	<div class="row">
    <form class="col s12">
      <div class="row">
        <div class="input-field col s6">
          <input id="first_name" name="firstname" type="text" class="validate">
          <label for="first_name">First Name</label>
        </div>
        <div class="input-field col s6">
          <input id="last_name" type="text" name="lastname" class="validate">
          <label for="last_name">Last Name</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="email" type="email"name="email" class="validate" >
          <label for="email">Email</label>
        </div>
      </div>
      <div class="row">
            <div class="input-field col s12">
          <textarea id="textarea1"name="textarea" class="materialize-textarea"></textarea>
          <label for="textarea1">Message</label>
        </div>
      </div>

      <button class="btn waves-effect waves-light grey darken-4" type="submit" name="action" value="Link index" onClick="a href="index.php"">Submit
    <i class="material-icons right">send</i>
     </button>

    </form>
  </div>



<h3 class="kontaktai">contact</h3>
	<p class="emailas"> lina.zabinskaja@gmail.com</p>	

<?php include "footer.php"; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>


</body>
</html>